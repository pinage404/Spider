#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module will parse a web page and extract the data"""

from spider import Spider


if __name__ == '__main__':
    ARG_PARSER = Spider.get_argument_parser_crawler()
    ARG = ARG_PARSER.parse_args()

    SPIDER = Spider(output_folder=ARG.output)

    if ARG.url:
        print("Extract")
        SPIDER.crawl_list(ARG.url,
                          max_depth=ARG.depth,
                          go_outside=ARG.outside)
    else:
        ARG_PARSER.print_help()
