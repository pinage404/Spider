#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This Module define Page Class Information"""

from json import JSONEncoder
from json import load as json_load
from json import dump as json_dump
import os
import re


class Page(dict):
    """Page's Data"""

    disallowed_filename_char = re.compile(r"[^a-zA-Z0-9]")

    def __init__(self, url="", title="", description="", keywords=None, links=None):
        """Page Object Construct"""
        self["url"] = url
        self["title"] = title
        self["description"] = description
        if keywords:
            self["keywords"] = set(keywords)
        else:
            self["keywords"] = set()
        if links:
            # \m/
            self["links"] = set(links)
        else:
            self["links"] = set()

        super().__init__()

    def to_dict(self):
        """ Return Page information like url, title, description...
            :param arg1: self
            :type arg1: Page
            :return: Return a dict with page information
            :rtype: dict
        """
        return {
            "url": self["url"],
            "title": self["title"],
            "description": self["description"],
            "keywords": self["keywords"],
            "links": self["links"]
        }

    def __str__(self):
        """ Return Page's url
            :param arg1: self
            :type arg1: Page
            :return: Return a dict with URL String
            :rtype: String
        """
        return self["url"]

    def __repr__(self):
        """ Return Page's information like url, title, description...
            :param arg1: self
            :type arg1: Page
            :return: Return a String with Page object information
            :rtype: String
        """
        return ("Page\n"
                + "(\n"
                + "\turl: " + self["url"] + "\n"
                + "\ttitle: " + self["title"] + "\n"
                + "\tdescription: " + self["description"] + "\n"
                + "\tkeywords: " + ", ".join(self["keywords"]) + "\n"
                + ")\n")

    @classmethod
    def _url_to_file_name(cls, url):
        """ Return a String name with URL
            :param arg1: cls
            :type arg1: Page
            :param arg2: url
            :type arg2: String
            :return: Return a String name with URL
            :rtype: String
        """
        return cls.disallowed_filename_char.sub("", url)

    @classmethod
    def _url_to_file_path(cls, folder, url):
        """ Use url to make a file name for savefile
            :param arg1: cls
            :type arg1: Page
            :param arg2: folder
            :type arg2: String
            :param arg3: url
            :type arg3: String
            :return: Return a os path file String .json
            :rtype: String
        """
        filename = cls._url_to_file_name(url)
        return os.path.join(folder, filename + ".json")

    def save(self, folder):
        """ Save Page information in an specific file location
            :param arg1: self
            :type arg1: Page
            :param arg2: folder
            :type arg2: String
            :return: Return Page Object
            :rtype: Page
        """
        file_path = self._url_to_file_path(folder, self["url"])
        with open(file_path, "w") as file_output:
            json_dump(self, file_output, cls=PageJSONEncoder, indent=True)
        return self

    @classmethod
    def exists(cls, folder, url):
        """ Check if folder and file exists
            :param arg1: cls
            :type arg1: Page
            :param arg2: folder
            :type arg2: String
            :param arg3: url
            :type arg3: String
            :return: Return True or False if filePath exists
            :rtype: Boolean
        """
        file_path = cls._url_to_file_path(folder, url)
        return (os.path.exists(file_path)
                and os.stat(file_path).st_size != 0)

    @classmethod
    def load(cls, folder, filename):
        """ Load information from the JSON file
            :param arg1: cls
            :type arg1: Page
            :param arg2: folder
            :type arg2: String
            :param arg3: filename
            :type arg3: String
            :return: Return Page Object
            :rtype: Page
        """
        file_path = os.path.join(folder, filename)
        if ((os.path.exists(file_path)
             and os.stat(file_path).st_size != 0)):
            with open(file_path, "r") as file_input:
                data = json_load(file_input)
                return cls(**data)


class PageJSONEncoder(JSONEncoder):
    """JSON page encoder"""

    def default(self, obj):
        """ Method to serialize additional types from JSONEncoder Class
            warnings:: https://bitbucket.org/logilab/pylint/issue
                       /414/false-positive-for-e0202-method-hidden
        """
        if isinstance(obj, set):
            return list(obj)

        return JSONEncoder.default(self, obj)
