#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    The spider know the web, it can browse
    the web and find something in it
    (like a bug or like a virgin ...)
"""

from argparse import ArgumentParser
from bs4 import BeautifulSoup
from traceback import print_exc
from urllib import parse as urlparse
from urllib import request as urlrequest
from urllib.error import HTTPError
from urllib.error import URLError
import os
import re
import sys

from page import Page
from link_queue import LinkQueue


class Spider:
    """A tool to crawl a web page and extract data"""

    @staticmethod
    def get_url_content(url):
        """ Open the URL url and create a Page Object
            :param arg1: url
            :type arg1: String
            :return: Return the content response
            :rtype: String
        """
        request = urlrequest.Request(url)
        with urlrequest.urlopen(request) as response:
            return response.read()

    @staticmethod
    def extract(url):
        """ Init a Page Object with data from extract url
            :param arg1: url
            :type arg1: String
            :return: Return the new Page Object
            :rtype: Page
        """
        page = Page(url)

        html = Spider.get_url_content(url)
        soup = BeautifulSoup(html, 'html.parser')

        if ((soup.title is not None
             and soup.title.contents)):
            page["title"] = soup.title.contents[0].strip()

        meta_list = soup.find_all("meta")
        for meta in meta_list:
            if (("name" in meta.attrs
                 and "content" in meta.attrs)):
                if meta.attrs["name"].lower() == "description":
                    page["description"] = meta.attrs["content"].strip()
                elif meta.attrs["name"].lower() == "keywords":
                    page["keywords"] = meta.attrs["content"].split(",")

        base_url = urlparse.urlsplit(url).geturl()
        base = soup.find("base")
        if ((base
             and "href" in base.attrs)):
            base_url = base.attrs["href"]

        for link in soup.find_all('a'):
            if "href" in link.attrs:
                url = link.attrs["href"]
                # if ".." in url:
                #     print("base_url", base_url, "url", url)
                url = urlparse.urljoin(base_url, url)
                # if ".." in url:
                #     print("base_url", base_url, "url", url)
                page["links"].add(url)

        # print("title", page["title"])
        # print("description", page["description"])
        # print("keywords", page["keywords"])
        # print("links", page["links"])

        return page

    @staticmethod
    def get_domain(url):
        """ Return domain string from url set in attribute
            :param arg1: url
            :type arg1: String
            :return: Return domain string
            :rtype: String
        """
        domain = urlparse.urlsplit(url).netloc
        match = re.search(r"(?:[^/.]+\.)?[^/.]+$", domain)
        if match:
            domain = match.group(0)
        return domain

    def __init__(self,
                 input_folder="results",
                 output_folder="results",
                 keep_query=False, keep_hash=False):
        self.url_queue = LinkQueue(keep_query=keep_query, keep_hash=keep_hash)
        self.input_folder = input_folder
        self.output_folder = output_folder

        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

    def _get_next_url(self):
        """ Loop while Spider stack get an url
            Remove and return an element from the left side of the deque
            If no elements are present, raises an IndexError
            :param arg1: self
            :type arg1: Spider
            :return: Return an element from the left side of the deque
        """
        while self.url_queue:
            yield self.url_queue.popleft()

    def _crawl(self, max_depth=2, go_outside=True):
        # init
        """ Set depth status
            and if the crawler can go outside the domain or not
            :param arg1: self
            :type arg1: Spider
            :param arg2: max_depth
            :type arg2: Int
            :param arg3: go_outside
            :type arg3: Boolean
        """
        domain = None
        prev = 0

        for url, depth in self._get_next_url():
            # needed by domain limit
            if depth == 0:
                domain = self.get_domain(url)

            # check depth limit
            if depth <= max_depth:
                # print depth and url
                url_extract = str(depth) + " : " + url
                url_extract += " " * int(prev - len(url_extract))
                prev = len(url_extract)
                sys.stdout.write(url_extract)
                sys.stdout.flush()

                try:
                    # extract data
                    page = Spider.extract(url).save(self.output_folder)

                    # add extracted links to queue
                    depth += 1
                    for link in page["links"]:
                        if ((link not in self.url_queue
                             and not Page.exists(self.output_folder, link)
                             and(go_outside or domain == self.get_domain(link)))):
                            self.url_queue.add(link, depth=depth)

                    # clear the depth and url
                    sys.stdout.write("\b" * prev)
                except HTTPError as err:
                    print(os.linesep + str(err) + os.linesep)
                except (UnicodeEncodeError, URLError, OSError):
                    print()
                    print_exc()
                    print()

        print()

    def crawl_list(self, url_list, **kwarg):
        """ Parse and index all the url, set in command line
            :param arg1: self
            :type arg1: Spider
            :param arg2: url_list
            :type arg2: list
            :param arg3: kwarg
            :type arg3:
        """
        for url in url_list:
            self.url_queue.add(url)

        self._crawl(**kwarg)

    def search(self, search):
        """ Find KEYWORD in Page Object
            :param arg1: self
            :type arg1: Spider
            :param arg2: search
            :type arg2: String
        """
        search = search.lower()

        for filename in os.listdir(self.input_folder):
            page = Page.load(self.input_folder, filename)
            try:
                if (search in page["url"].lower()
                        or search in page["title"].lower()
                        or search in page["description"].lower()
                        or search in ", ".join(page["keywords"]).lower()):
                    yield page
            except IOError:
                pass

    @staticmethod
    def get_argument_parser():
        """
            Describe application utilities
        """
        arg_parser = ArgumentParser(description="A small web crawler")
        return arg_parser

    @classmethod
    def get_argument_parser_crawler(cls, arg_parser=None):
        """
            Define argument from command line for crawler method
        """
        if not arg_parser:
            arg_parser = cls.get_argument_parser()

        arg_parser.add_argument("url",
                                type=str,
                                nargs="*",
                                help="The first URL to parse (e.g. http://google.com)")
        arg_parser.add_argument("--depth",
                                "-d",
                                type=int,
                                default=2,
                                metavar="MAX_DEPTH_LEVEL",
                                help="The depth to crawl")
        arg_parser.add_argument("--outside",
                                action="store_true",
                                default=False,
                                help="Allow / disallow to crawl other domains")
        arg_parser.add_argument("--output",
                                "-o",
                                "--save",
                                type=str,
                                metavar="FOLDER_PATH",
                                default="results",
                                help="File path where save data")
        arg_parser.add_argument("--keep-query",
                                "--keep_query",
                                action="store_true",
                                default=False,
                                help="Keep the query in the url")
        arg_parser.add_argument("--keep-hash",
                                "--keep_hash",
                                action="store_true",
                                default=False,
                                help="Keep the hash in the url")
        return arg_parser

    @classmethod
    def get_argument_parser_search(cls, arg_parser=None):
        """
            Define argument from command line for search method
        """
        if not arg_parser:
            arg_parser = cls.get_argument_parser()

        arg_parser.add_argument("--input",
                                "-i",
                                "--load",
                                type=str,
                                metavar="FOLDER_PATH",
                                default="results",
                                help="File path where restore data")
        arg_parser.add_argument("--search",
                                "-s",
                                "--keyword",
                                "-k",
                                "--query",
                                "-q",
                                type=str,
                                metavar="KEYWORD",
                                help="Search in the dict")
        return arg_parser

    @classmethod
    def main(cls):
        """
            Main Spider Programm Function
        """
        arg_parser = cls.get_argument_parser_crawler()
        arg_parser = cls.get_argument_parser_search(arg_parser)

        arg_parsed = arg_parser.parse_args()

        spider = cls(input_folder=arg_parsed.input,
                     output_folder=arg_parsed.output,
                     keep_query=arg_parsed.keep_query,
                     keep_hash=arg_parsed.keep_hash)

        if arg_parsed.url:
            print("Extract")
            spider.crawl_list(arg_parsed.url,
                              max_depth=arg_parsed.depth,
                              go_outside=arg_parsed.outside)

        if arg_parsed.search:
            print("Search")
            for page in spider.search(arg_parsed.search):
                print(page)

        if (not arg_parsed.url
            and not arg_parsed.search):
            arg_parser.print_help()

if __name__ == '__main__':
    Spider.main()
