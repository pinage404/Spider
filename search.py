#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This module will search a keyword from extracted data of the web pages"""

from spider import Spider


if __name__ == '__main__':
    ARG_PARSER = Spider.get_argument_parser_search()
    ARG = ARG_PARSER.parse_args()

    SPIDER = Spider(input_folder=ARG.input,
                    keep_query=ARG.keep_query,
                    keep_hash=ARG.keep_hash)

    if ARG.search:
        keyword = ARG.search
    else:
        keyword = input("Tape the keyword : ")

    if keyword \
    and keyword.strip() != "":
        print("Search")
        for page in SPIDER.search(keyword):
            print(page)
    else:
        ARG_PARSER.print_help()
