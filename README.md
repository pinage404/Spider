# Spider
<!-- MarkdownTOC -->

- [Description](#description)
- [Install](#install)
- [Usage](#usage)
	- [Crawler](#crawler)
		- [Help](#help)
		- [Parse and save data](#parse-and-save-data)
	- [Search](#search)
		- [Help](#help-1)
		- [Restore data and search a keyword](#restore-data-and-search-a-keyword)
	- [Spider](#spider)
		- [Help](#help-2)
		- [All in one](#all-in-one)
- [By](#by)
- [Done](#done)

<!-- /MarkdownTOC -->



## Description
This program is a web crawler



## Install
	aptitude install python3
	pip install beautifulsoup4
	chmod +x spider.py
	python3 ./spider.py --help



## Usage
### Crawler
#### Help
	python3 ./crawler.py --help

#### Parse and save data
	python3 ./crawler.py https://google.com --output extracted_data --keep_query --depth 1


### Search
#### Help
	python3 ./search.py --help

#### Restore data and search a keyword
	python3 ./search.py --input extracted_data --keyword google


### Spider
spider.py have all availlable options of crawler.py and search.py

#### Help
	python3 ./spider.py --help

#### All in one
	python3 ./spider.py https://google.com http://estcequecestbientotleweekend.fr/ http://perdu.com --depth 1 --outside --output extracted_data --keep-query --keep-hash --input extracted_data --keyword google



## By
- Mélody Sholtes
- Aurélien Ianni
- Bruno Ebstein



## Done
- extract() method
- crawl() method
- crawl without option
- crawl with option
- depth limit
- parcour with instead depth (using collections.deque)
- outside domain limit
- save all data into one folder
- restore data from one folder
- search a keyword
- remove query and hash from link
