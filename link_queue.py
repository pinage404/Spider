#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module define LinkQueue Class Information"""

from collections import deque
import re


class LinkQueue(deque):
    """ Structure to queue link
        warning::avoid unknow protocol like "mailto" or "javascript"
    """
    pattern_valid_url = re.compile(r"^(s?(?:ftp|http)s?:)?//")

    pattern_white_space = re.compile(r"[\0\n\r\t]")
    pattern_query = re.compile(r"\?(?:[^#]*)(?=#.*)$")
    pattern_hash = re.compile(r"#(?:.*)$")

    def __init__(self, keep_query=False, keep_hash=False):
        """ LinkQueue Object Construct"""
        self.keep_query = keep_query
        self.keep_hash = keep_hash
        super().__init__()

    def add(self, link, depth=0):
        """Add a new url to the queue list"""
        # delete the white space in the URL
        link = self.pattern_white_space.sub("", link)

        # delete the query in the URL
        if not self.keep_query:
            link = self.pattern_query.sub("", link)

        # delete the hash in the URL
        if not self.keep_hash:
            link = self.pattern_hash.sub("", link)

        if ((link != ""
             and self.pattern_valid_url.search(link))):
            self.append((link, depth))

    def __contains__(self, searched_link):
        """ Check if the link in Attribute is contain in the list"""
        for line in self:
            if line[0] == searched_link:
                return True

        return False
